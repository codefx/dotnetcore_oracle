﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DotNetCoreOracle.Tools
{
    public class ConfigHelper
    {

        /// <summary>
        /// this is  for  to get the "connection string"  to Oracle databases from a config file configured by you, In DotnetCore 2.1. 
        /// 
        /// </summary>        
        /// <returns>this returns "config string" value from "Json_configFileName.json" in the Project  Folder</returns>
        public string GetConfig(string name="",string Json_configFileName)
        {
            var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile(Json_configFileName, false)
            .Build();

            string config = configuration.GetSection(name).Value;

            if (string.IsNullOrEmpty(config))
                throw new ArgumentException("No config string in config.json");
            return config;
        }
    }
}

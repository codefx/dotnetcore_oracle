﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text;

namespace DotNetCoreOracle.Tools
{
    public class DotNetCoreOracleHelper
    {
        public List<YourModel> FillViewModel<YourModel>(string sqlCommand, string conStr) where YourModel : class, new()
        {
            OracleDataAdapter da = new OracleDataAdapter(sqlCommand, conStr);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return ConvertDataTableToList<YourModel>(dt);
        }
        /// <summary>
        /// only convert datatable to list
        /// </summary>
        /// <typeparam name="YourModel"></typeparam>
        /// <param name="dt">to convert datatable</param>
        /// <returns>returns list of your model</returns>
        public List<YourModel> ConvertDataTableToList<YourModel>(DataTable dt) where YourModel : class, new()
        {

            var ListT = new List<YourModel>();

            foreach (DataRow row in dt.Rows)
            {
                var tdn = new YourModel();
                foreach (var prop in tdn.GetType().GetProperties())
                {
                    try
                    {
                        PropertyInfo propertyInfo = tdn.GetType().GetProperty(prop.Name);
                        propertyInfo.SetValue(tdn, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                    }
                    catch
                    {
                        continue;
                    }
                }
                ListT.Add(tdn);
            }
            return ListT;
        }
    }
}
